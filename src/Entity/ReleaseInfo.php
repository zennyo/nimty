<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReleaseRepository")
 */
class ReleaseInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(type="integer")
     */
    private $is_visible;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderInfo", inversedBy="releases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $order_info;

    /**
     * @ORM\Column(type="string", length=4096, nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=4096, nullable=true)
     */
    private $changelog;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(?\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getIsVisible(): ?int
    {
        return $this->is_visible;
    }

    public function setIsVisible(int $is_visible): self
    {
        $this->is_visible = $is_visible;

        return $this;
    }

    public function getOrderInfo(): ?OrderInfo
    {
        return $this->order_info;
    }

    public function setOrderInfo(?OrderInfo $order_info): self
    {
        $this->order_info = $order_info;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getChangelog(): ?string
    {
        return $this->changelog;
    }

    public function setChangelog(?string $changelog): self
    {
        $this->changelog = $changelog;

        return $this;
    }
}
