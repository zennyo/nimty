<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account implements UserInterface {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $associated_vk;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $associated_tg;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\University", inversedBy="accounts")
     */
    private $university;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account")
     */
    private $referrer;

    /**
     * @ORM\Column(type="integer")
     */
    private $access_level;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderInfo", mappedBy="account")
     */
    private $order_infos;

    /**
     * @ORM\Column(type="string", length=4096, nullable=true)
     */
    private $comment;

    public function __construct($id) {
        $this->order_infos = new ArrayCollection();
        $this->id = (int) $id;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getAssociatedVk(): ?int {
        return $this->associated_vk;
    }

    public function setAssociatedVk(?int $associated_vk): self {
        $this->associated_vk = $associated_vk;

        return $this;
    }

    public function getAssociatedTg(): ?int {
        return $this->associated_tg;
    }

    public function setAssociatedTg(?int $associated_tg): self {
        $this->associated_tg = $associated_tg;

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getUniversity(): ?University {
        return $this->university;
    }

    public function setUniversityId(?University $university_id): self {
        $this->university = $university_id;

        return $this;
    }

    public function getReferrer(): ?self {
        return $this->referrer;
    }

    public function setReferrerId(?self $referrer_id): self {
        $this->referrer = $referrer_id;

        return $this;
    }

    public function getAccessLevel(): ?int {
        return $this->access_level;
    }

    public function setAccessLevel(int $access_level): self {
        $this->access_level = $access_level;

        return $this;
    }

    /**
     * @return Collection|OrderInfo[]
     */
    public function getOrderInfos(): Collection {
        return $this->order_infos;
    }

    public function addOrderInfo(OrderInfo $order): self {
        if (!$this->order_infos->contains($order)) {
            $this->order_infos[] = $order;
            $order->setAccount($this);
        }

        return $this;
    }

    public function removeOrderInfo(OrderInfo $order): self {
        if ($this->order_infos->contains($order)) {
            $this->order_infos->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getAccount() === $this) {
                $order->setAccount(null);
            }
        }

        return $this;
    }

    public function getRoles(): array {
        switch ($this->access_level) {
            case 0:
                return ["ROLE_CLIENT"];
            case 1:
                return ["ROLE_CLIENT", "ROLE_PARTNER"];
            case 2:
                return ["ROLE_CLIENT", "ROLE_PARTNER", "ROLE_ADMIN"];
        }
        return [];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string|null The encoded password if any
     */
    public function getPassword() {
        // TODO: Implement getPassword() method.
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt() {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername() {
        return (string)$this->getId();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials() {
        // TODO: Implement eraseCredentials() method.
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
