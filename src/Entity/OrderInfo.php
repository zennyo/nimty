<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderInfoRepository")
 */
class OrderInfo {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="date")
     */
    private $start_date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $price_paid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReleaseInfo", mappedBy="order_info")
     */
    private $release_infos;

    public function __construct()
    {
        $this->release_infos = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getAccount(): ?Account {
        return $this->account;
    }

    public function setAccount(?Account $account_id): self {
        $this->account = $account_id;

        return $this;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getState(): ?int {
        return $this->state;
    }

    public function setState(int $state): self {
        $this->state = $state;

        return $this;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(?string $code): self {
        $this->code = $code;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self {
        $this->start_date = $start_date;

        return $this;
    }

    public function getPrice(): ?int {
        return $this->price;
    }

    public function setPrice(?int $price): self {
        $this->price = $price;

        return $this;
    }

    public function getPricePaid(): ?int {
        return $this->price_paid;
    }

    public function setPricePaid(int $price_paid): self {
        $this->price_paid = $price_paid;

        return $this;
    }

    public function toArrayForRoles($roles = ["RULE_USER"]) {
        $releases = [];
        foreach ($this->getReleaseInfos() as $release) {
            $releases[] = $release->toArrayForRoles($roles);
        }
        $result = [
            "id" => $this->getId(),
            "code" => $this->getCode(),
            "title" => $this->getTitle(),
            "start_date" => $this->getStartDate(),
            "state" => $this->getState(),
            "price" => $this->getPrice(),
            "price_paid" => $this->getPricePaid(),
            "releases" => $releases,
        ];
        return $result;
    }

    /**
     * @return Collection|ReleaseInfo[]
     */
    public function getReleaseInfos(): Collection
    {
        return $this->release_infos;
    }

    public function addRelease(ReleaseInfo $release): self
    {
        if (!$this->release_infos->contains($release)) {
            $this->release_infos[] = $release;
            $release->setOrderInfo($this);
        }

        return $this;
    }

    public function removeRelease(ReleaseInfo $release): self
    {
        if ($this->release_infos->contains($release)) {
            $this->release_infos->removeElement($release);
            // set the owning side to null (unless already changed)
            if ($release->getOrderInfo() === $this) {
                $release->setOrderInfo(null);
            }
        }

        return $this;
    }
}
