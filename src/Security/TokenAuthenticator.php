<?php
// src/Security/TokenAuthenticator.php
namespace App\Security;

use App\Entity\Account;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator {
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public static function createToken($id) {
        $time = time();
        $secret = getenv("API_KEY_SECRET");
        return "$id:$time:".hash("sha256", "$id{nimty}$time{nimty}$secret");
    }

    public function supports(Request $request) {
        return $request->headers->has('X-AUTH-TOKEN');
    }

    public function getCredentials(Request $request) {
        return [
            'token' => $request->headers->get('X-AUTH-TOKEN'),
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider) {
        $token = $credentials['token'];

        $parts = explode(":", $token);
        if (count($parts) != 3)
            return null;
        $id = $parts[0];
        $time = $parts[1];
        $signature = $parts[2];
        $secret = getenv("API_KEY_SECRET");
        if (hash("sha256", "$id{nimty}$time{nimty}$secret") != $signature) {
            return null;
        }
        if ((int)$time < time() - 86400) {
            return null;
        }

        return $this->em->getRepository(Account::class)
            ->findOneBy(['id' => $id]);
    }

    public function checkCredentials($credentials, UserInterface $user) {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case

        // return true to cause authentication success
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        $data = [
            'status' => 'error',
            'token_refresh' => true,
            'error_message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null) {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe() {
        return false;
    }
}
