<?php


namespace App\Security;


use App\Entity\Account;
use App\Entity\OrderInfo;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OrderVoter extends Voter {

    protected function supports(string $attribute, $subject) {
        if (!($subject instanceof OrderInfo))
            return false;
        if (!in_array($attribute, ["view", "edit"]))
            return false;
        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token) {
        $user = $token->getUser();

        if (!($user instanceof Account)) {
            return false;
        }

        if (in_array("ROLE_ADMIN", $user->getRoles())) {
            return true;
        }

        if ($attribute == "edit") {
            return false;
        }

        /** @var OrderInfo $subject */
        if ($subject->getAccount() == $user) {
            return true;
        }

    }
}
