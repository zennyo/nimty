<?php


namespace App\Exceptions;

use Exception;

class ApiResponseErrorException extends Exception {
    public function __construct($message) {
        parent::__construct($message);
    }
}