<?php

namespace App\Controller\ClientArea;

use App\Entity\Account;
use App\Exceptions\ApiResponseErrorException;
use App\Kernel;
use App\Security\TokenAuthenticator;
use App\Utils\ApiResponse;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Core\Security;

class AuthController extends AbstractController {

    /**
     * @Route("/client_area/auth/telegram", name="client_area_auth_telegram")
     * @param Request $req
     * @return Response
     */
    public function telegram(Request $req) {
        try {
            $user_object = json_decode($req->getContent(), true)["user_object"];
            $this->checkTelegramAuthorization($user_object);
            return new JsonResponse(["token" => "sldkfj"]);
        } catch (Exception $e) {
            return new JsonResponse(["error_message" => $e->getMessage()], Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @Route("/client_area/auth/code", name="client_area_auth_code")
     * @param Request $req
     * @param TokenAuthenticator $authenticator
     * @param GuardAuthenticatorHandler $guardHandler
     * @return Response
     * @throws ApiResponseErrorException
     */
    public function code(Request $req, TokenAuthenticator $authenticator, GuardAuthenticatorHandler $guardHandler) {
        $code = json_decode($req->getContent(), true)["code"];
        if ($code == $_ENV["AUTH_LOGIN_CODE"]) {
            $em = $this->getDoctrine()->getManager();
            $accounts_r = $em->getRepository(Account::class);
            /** @var Account|null $first_account */
            $first_account = $accounts_r->find(1);
            $guardHandler->authenticateUserAndHandleSuccess($first_account, $req, $authenticator, "main");
            return new ApiResponse(["token" => TokenAuthenticator::createToken("2")]);
        } else {
            throw new ApiResponseErrorException("Invalid code");
        }
    }

    /**
     * @Route("/client_area/getMe", name="client_area_auth_get_me")
     * @param Security $security
     * @return Response
     * @throws Exception
     */
    public function getMe(Security $security) {
        /** @var Account $user */
        $user = $security->getUser();
        if ($user == null) {
            throw new Exception("User is not authenticated");
        }
        $university = $user->getUniversity();
        return new ApiResponse([
            "name" => $user->getName(),
            "university" => ($university !== null) ? $university->getName() : null,
            "access_level" => $user->getAccessLevel(),
            "associated_vk" => $user->getAssociatedVk(),
            "associated_tg" => $user->getAssociatedTg(),
        ]);
    }

    /**
     * @param $auth_data
     * @throws Exception
     */
    private function checkTelegramAuthorization($auth_data) {
        $check_hash = $auth_data['hash'];
        unset($auth_data['hash']);
        $data_check_arr = [];
        foreach ($auth_data as $key => $value) {
            $data_check_arr[] = $key . '=' . $value;
        }
        sort($data_check_arr);
        $data_check_string = implode("\n", $data_check_arr);
        $secret_key = hash('sha256', $_ENV["TELEGRAM_BOT_TOKEN"], true);
        $hash = hash_hmac('sha256', $data_check_string, $secret_key);
        if (strcmp($hash, $check_hash) !== 0) {
            throw new Exception("Invalid signature");
        }
        if ((time() - $auth_data['auth_date']) > 86400) {
            throw new Exception("Data is too old");
        }
    }

}
