<?php


namespace App\Controller\ClientArea;


use App\Entity\Account;
use App\Entity\OrderInfo;
use App\Repository\OrderInfoRepository;
use App\Utils\ApiResponse;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class OrdersController extends AbstractController {

    /**
     * @Route("/client_area/orders/list", name="client_area_order_list")
     * @param Security $security
     * @return Response
     */
    public function list(Security $security) {
        /** @var Account $user */
        $user = $security->getUser();
        $array = [];
        foreach ($user->getOrderInfos() as $order) {
            $array[] = $order->toArrayForRoles($user->getRoles());
        }
        return new ApiResponse(["orders" => $array]);
    }

    /**
     * @Route("/client_area/orders/get/{id}", name="client_area_order_get", requirements={"page"="\d+"})
     * @param int $id
     * @param Security $security
     * @return Response
     */
    public function orderGet(Security $security, int $id) {
        try {
            /** @var Account $user */
            $user = $security->getUser();
            $order = $this->getDoctrine()->getRepository(OrderInfo::class)->find($id);
            if ($order == null) {
                throw new Exception("Order not found");
            }
            $this->denyAccessUnlessGranted("view", $order);
            return new JsonResponse(["order" => $order->toArrayForRoles($user->getRoles())]);
        } catch (Exception $e) {
            echo $e->getTraceAsString();
            return new JsonResponse(["error_message" => $e->getMessage()], Response::HTTP_FORBIDDEN);
        }
    }
}