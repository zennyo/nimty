<?php


namespace App\Utils;



use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse {
    public function __construct($data = null) {
        parent::__construct([
            "status" => "ok",
            "data" => $data,
        ]);
    }

}