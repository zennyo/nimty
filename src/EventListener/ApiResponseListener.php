<?php

namespace App\EventListener;

use App\Exceptions\ApiResponseErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ApiResponseListener {
    public function onKernelException(ExceptionEvent $event) {
        $exception = $event->getThrowable();

        if ($exception instanceof ApiResponseErrorException) {
            $response = new JsonResponse([
                "status" => "error",
                "error_message" => $exception->getMessage(),
            ]);
            $event->allowCustomResponseCode();
            $event->setResponse($response);
        }
    }

}